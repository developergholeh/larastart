<?php

namespace App\Http\Controllers\Admin\roles;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Roles as Mrole;
use View;
use Input;
use Session;
class Roles extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $role = Mrole::all();

        // load the view and pass the nerds
        return View::make('roles.index')
            ->with('role', $role);
       // return View('roles.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return View::make('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request, [
            'name' => 'required'
        ]);


        // store
        $role = new Mrole;
        $role->name= $request['name'];//Input::get('name');
        $role->save();
        // redirect
         Session::flash('message', 'Successfully created role!');
        return redirect('admin/role');

        // }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // get the nerd
        $role = Mrole::find($id);

        // show the view and pass the nerd to it
        return View::make('roles.show')
            ->with('role', $role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $role = Mrole::find($id);

        // show the edit form and pass the nerd
        return View::make('roles.edit')
            ->with('role', $role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name' => 'required'
        ]);


        // store
        $role = Mrole::find($id);
        $role->name= $request['name'];//Input::get('name');
        $role->save();
        // redirect
        Session::flash('message', 'Successfully Updated role');
        return redirect('admin/role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $role=Mrole::find($id);
        $role->delete();
        // redirect
        Session::flash('message', 'Successfully deleted the role');
        return redirect('admin/role');
    }
}
