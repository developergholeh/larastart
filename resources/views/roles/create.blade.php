@extends('layouts.app')

@section('content')
    <div class="container">

    <h1>Create a Role</h1>

    {{ Form::open(array('url' => 'admin/role')) }}

    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
    </div>


    {{ Form::submit('Create the Role!', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection
