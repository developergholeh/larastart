@extends('layouts.app')

@section('content')
    <div class="container">

    <h2>Edit {{ $role->name }}</h2>


    {{ Form::model($role, array('route' => array('role.update', $role->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Edit the Role', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}


</div>
@endsection
