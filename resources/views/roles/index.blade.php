@extends('layouts.app')

@section('content')
    <div class="container">

    <h1>Roles</h1>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <table class="table table-striped table-bordered">
        <a class="btn btn-small btn-primary pull-right" href="{{ URL::to('admin/role/create') }}">Create a Role</a>
        <thead>
        <tr>
            <td>ID</td>
            <td>Role Name</td>
            <td>Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($role as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->name }}</td>

                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    {{ Form::open(array('url' => 'admin/role/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                    {{ Form::close() }}

                    <a class="btn btn-small btn-success pull-right" href="{{ URL::to('admin/role/' . $value->id) }}">Show</a>
                    <a class="btn btn-small btn-info pull-right" href="{{ URL::to('admin/role/' . $value->id . '/edit') }}">Edit</a>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>
@endsection
