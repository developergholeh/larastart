@extends('layouts.app')

@section('content')
    <div class="container">

    <h1>Showing {{ $role->name }}</h1>

    <div class="jumbotron text-center">
        <p>
            <strong>Role Name:</strong> {{ $role->name }}<br>
            <strong>Date Created:</strong> {{ $role->created_at }}
        </p>
    </div>


</div>

@endsection
